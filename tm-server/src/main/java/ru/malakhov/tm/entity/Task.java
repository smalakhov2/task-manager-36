package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    @ManyToOne
    private Project project;

    @NotNull
    @ManyToOne
    private User user;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public Task(
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final User user
    ) {
        this.name = name;
        this.description = description;
        this.user = user;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof Task && super.equals(o)) {
            @NotNull final Task task = (Task) o;
            return Objects.equals(name, task.name)
                    && Objects.equals(description, task.description)
                    && Objects.equals(user.id, task.user.id)
                    && Objects.equals(project.id, task.project.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, user.id, project.id);
    }

}