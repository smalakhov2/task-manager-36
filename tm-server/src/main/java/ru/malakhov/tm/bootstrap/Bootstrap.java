package ru.malakhov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.endpoint.AbstractEndpoint;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.xml.ws.Endpoint;

@Getter
@Setter
@Component
@DependsOn("propertyService")
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserService userService;

    private void initTestData() {
        try {
            @Nullable UserDto admin = userService.findOneDtoByLogin("admin");
            if (admin == null) userService.create("admin", "admin", Role.ADMIN);
            @Nullable UserDto user = userService.findOneDtoByLogin("test");
            if (user == null) userService.create("test", "test");
        } catch (AbstractException e) {
            System.out.println("Error loading test data.");
        }
    }

    private void initEndpoints() {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        for (@NotNull final AbstractEndpoint endpoint : endpoints) {
            @NotNull final String name = endpoint.getClass().getSimpleName();
            @NotNull final String url = String.format("http://%s:%d/%s?wsdl", host, port, name);
            System.out.println(url);
            Endpoint.publish(url, endpoint);
        }
    }

    public void run() {
        initTestData();
        try {
            initEndpoints();
        } catch (Exception e) {
            throw new RuntimeException("Error! Endpoints, weren't load...", e);
        }
    }

}