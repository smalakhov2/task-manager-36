package ru.malakhov.tm.repository.entity;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.entity.Task;

import java.util.Optional;

@Repository
public interface TaskRepository extends AbstractEntityRepository<Task> {

    @Query("SELECT e FROM Task e WHERE e.user.id=:userId")
    Iterable<Task> findByUserId(@Param("userId") String userId);

    @Query("SELECT e FROM Task e WHERE e.user.id=:userId ORDER BY e.name ASC")
    Iterable<Task> findByUserIdOrderByName(@Param("userId") String userId);

    @Query("SELECT e FROM Task e WHERE e.id=:id AND e.user.id=:userId")
    Optional<Task> findByIdAndUserId(
            @Param("id") String id,
            @Param("userId") String userId
    );

    @Query("SELECT e FROM Task e WHERE e.user.id=:userId AND e.name=:name")
    Optional<Task> findByUserIdAndName(
            @Param("userId") String userId,
            @Param("name") String name
    );

    @Modifying
    @Transactional
    @Query("DELETE Task e WHERE e.user.id=:userId")
    int deleteByUserId(@Param("userId") String userId);

    @Modifying
    @Transactional
    @Query("DELETE Task e WHERE e.id=:id AND e.user.id=:userId")
    int deleteByIdAndUserId(
            @Param("id") String id,
            @Param("userId") String userId
    );

    @Modifying
    @Transactional
    @Query("DELETE Task e WHERE e.user.id=:userId AND e.name=:name")
    int deleteByUserIdAndName(
            @Param("userId") String userId,
            @Param("name") String name
    );

}