package ru.malakhov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IndexIncorrectException;
import ru.malakhov.tm.repository.dto.ProjectDtoRepository;
import ru.malakhov.tm.repository.entity.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@Setter
@Service
public final class ProjectService extends AbstractService<ProjectDto, ProjectDtoRepository> implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    public ProjectService(@NotNull final ProjectDtoRepository projectDtoRepository) {
        super(projectDtoRepository);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDto project = new ProjectDto(name, "", userId);
        save(project);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDto project = new ProjectDto(name, description, userId);
        save(project);
    }

    @NotNull
    @Override
    public List<Project> findAllEntity() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAllDtoByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        dtoRepository.findByUserIdOrderByName(userId).forEach(projects::add);
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAllEntityByUserId(
            @Nullable final String userId
    ) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<Project> projects = new ArrayList<>();
        projectRepository.findByUserIdOrderByName(userId).forEach(projects::add);
        return projects;
    }

    @Nullable
    @Override
    public Project findOneEntityByIdAndUserId(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return dtoRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    @Nullable
    @Override
    public Project findOneEntityByIdAndUserId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {

        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final int counter = (int) dtoRepository.count();
        if (index == null || index < 0 || index >= counter) throw new IndexIncorrectException();
        @NotNull final List<ProjectDto> projects = new ArrayList<>();
        dtoRepository.findByUserIdOrderByName(userId).forEach(projects::add);
        return projects.get(index);
    }

    @Nullable
    @Override
    public Project findOneEntityByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final int counter = (int) dtoRepository.count();
        if (index == null || index < 0 || index >= counter) throw new IndexIncorrectException();
        @NotNull final List<Project> projects = new ArrayList<>();
        projectRepository.findByUserIdOrderByName(userId).forEach(projects::add);
        return projects.get(index);
    }

    @Nullable
    @Override
    public ProjectDto findOneDtoByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return dtoRepository.findByUserIdAndName(userId, name).orElse(null);
    }

    @Nullable
    @Override
    public Project findOneEntityByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByUserIdAndName(userId, name).orElse(null);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) throws EmptyUserIdException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneByIdAndUserId(
            @Nullable final String id,
            @Nullable final String userId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final int counter = (int) dtoRepository.count();
        if (index == null || index < 0 || index >= counter) throw new IndexIncorrectException();
        @Nullable final Project project = findOneEntityByIndex(userId, index);
        if (project == null) return;
        projectRepository.deleteById(project.getId());
    }

    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDto project = findOneDtoByIdAndUserId(id, userId);
        if (project == null) return;
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

    @Override
    @Transactional
    public void updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @Nullable final ProjectDto project = findOneDtoByIndex(userId, index);
        if (project == null) return;
        project.setName(name);
        project.setDescription(description);
        save(project);
    }

}