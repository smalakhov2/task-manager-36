package ru.malakhov.tm.listener.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.AdminUserEndpoint;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.UserDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

import java.util.List;

@Component
public class UserListShowListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list.";
    }

    @Override
    @EventListener(condition = "@userListShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[USER-LIST]");
        @Nullable final SessionDto session = propertyService.getSession();
        @NotNull final List<UserDto> users = adminUserEndpoint.getAllUserList(session);
        int index = 1;
        for (@Nullable final UserDto user : users) {
            if (user != null) {
                System.out.println(index + ". " + user.getLogin());
                index++;
            }
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
