package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.event.RunEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.util.SystemUtil;

@Component
public class SystemInfoListener extends AbstractListener {

    @NotNull
    @Override
    public String name() {
        return "system-info";
    }

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about system.";
    }

    private void execute() {
        System.out.println("[SYSTEM INFO]");
        SystemUtil.showSystemInfo();
    }

    @Override
    @EventListener(condition = "@systemInfoListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        execute();
    }

    @EventListener(condition = "@systemInfoListener.arg() == #event.name")
    public void handler(@NotNull final RunEvent event) {
        execute();
    }

    @Override
    public boolean secure() {
        return false;
    }

}
