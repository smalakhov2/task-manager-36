package ru.malakhov.tm.api.service.dto;

import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.repository.dto.ProjectDtoRepository;

public interface IProjectDtoService extends IDtoService<ProjectDto, ProjectDtoRepository> {

    public void create();

}