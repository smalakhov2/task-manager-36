package ru.malakhov.tm.api.service.entity;

import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.repository.entity.TaskRepository;

public interface ITaskService extends IService<Task, TaskRepository> {
}