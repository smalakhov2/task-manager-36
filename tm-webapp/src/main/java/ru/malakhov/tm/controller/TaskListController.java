package ru.malakhov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.api.service.entity.ITaskService;

@Controller
@RequestMapping("/tasks")
public final class TaskListController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @GetMapping
    public ModelAndView viewProjectListGet() {
        return new ModelAndView("task_list", "tasks", taskService.findAll());
    }

}