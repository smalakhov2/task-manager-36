package ru.malakhov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malakhov.tm.dto.AbstractEntityDto;


public interface AbstractDtoRepository<E extends AbstractEntityDto> extends JpaRepository<E, String> {
}