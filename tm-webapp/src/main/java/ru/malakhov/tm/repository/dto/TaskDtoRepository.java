package ru.malakhov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.TaskDto;

@Repository
public interface TaskDtoRepository extends AbstractDtoRepository<TaskDto> {
}