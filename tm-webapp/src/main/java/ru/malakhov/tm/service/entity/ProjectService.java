package ru.malakhov.tm.service.entity;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.entity.IProjectService;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.repository.entity.ProjectRepository;

@Setter
@Service
public final class ProjectService extends AbstractService<Project, ProjectRepository> implements IProjectService {

    @Autowired
    public ProjectService(@NotNull final ProjectRepository projectRepository) {
        super(projectRepository);
    }

}